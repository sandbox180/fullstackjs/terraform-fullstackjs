terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 4.6.0"
    }
  }
}


resource "aws_security_group" "public_sg_elb" {
  name = var.sg_name
  description = var.sg_description
  vpc_id = var.vpc_id
  egress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    cidr_blocks = ["0.0.0.0/0"]

  }
  dynamic "ingress" {
    for_each = var.ingress_rules_cidr
    content {
      protocol = ingress.value["protocol"]
      from_port = ingress.value["from_port"]
      to_port = ingress.value["to_port"]
      cidr_blocks = ingress.value["cidr_blocks"]
    }
  }
  dynamic "ingress" {
    for_each = var.ingress_rules_sg
    content {
      protocol = ingress.value["protocol"]
      from_port = ingress.value["from_port"]
      to_port = ingress.value["to_port"]
      security_groups = ingress.value["security_groups"]
    }
  }
  tags = var.resources_tags
}
