terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 4.6.0"
    }
  }
}


data "aws_region" "current"{}
data "aws_caller_identity" "current" {}

locals {
  mongo_infos = {
    db = "${var.project_name}-db"
    db_test = "${var.project_name}-db-test"
    admin = "enzo"
    admin_pwd = "enzopwd"
    user = "api"
    userpwd = "apipwd"
    hostname = var.api_service_infos.mongo_ct.name
  }
}


resource "aws_ecs_task_definition" "task_api" {
  family = "${var.api_service_infos.prefix_name}-task-${var.api_service_infos.suffix_name}"
  execution_role_arn = aws_iam_role.ecs_task_execution.arn
  network_mode = "bridge"
  cpu = 300
  memory = 300
  requires_compatibilities = ["EC2"]
  container_definitions = jsonencode([
    {
      name = var.api_service_infos.api_ct.name
      links = [
        var.api_service_infos.mongo_ct.name
      ]
      image = var.api_service_infos.api_ct.image
      essential = true
      dependsOn = [
        {
          condition : "START",
          containerName : var.api_service_infos.mongo_ct.name
        }
      ]
      portMappings = [{
        containerPort = var.api_service_infos.api_ct.port
        hostPort      = 0
      }]
      healthCheck = {
        retries: 3
        command =["CMD-SHELL","curl -f http://127.0.0.1:${var.api_service_infos.api_ct.port}/${var.api_name}/healthcheck || exit 1"]
        timeout = 10
        interval = 60
        startPeriod = 10
      }
      logConfiguration = {
        logDriver = "awslogs"
        options = {
          awslogs-group = var.log_group_name
          awslogs-region = data.aws_region.current.name
          awslogs-stream-prefix = "api"
        }
      }
      environment = [
        { name: "API_PORT", value: tostring(var.api_service_infos.api_ct.port) },
        { name: "API_NAME", value: var.api_name },
        { name: "NODE_ENV", value: var.node_env },
        { name: "MONGO_PORT", value: tostring(var.api_service_infos.mongo_ct.port) },
        { name: "MONGO_HOSTNAME", value: local.mongo_infos.hostname},
        { name: "MONGO_API_DB", value: local.mongo_infos.db},
        { name: "MONGO_API_DB_USERNAME", value: local.mongo_infos.user},
        { name: "MONGO_API_DB_PASSWORD", value: local.mongo_infos.userpwd},
        { name: "MONGO_AUTH_SOURCE", value: local.mongo_infos.db},
        { name: "MONGO_TEST_HOSTNAME", value: local.mongo_infos.db_test},
      ]
    },
    {
      name  = var.api_service_infos.mongo_ct.name
      image  = var.api_service_infos.mongo_ct.image
      essential = true
      environment = [
        { name: "MONGO_INITDB_ROOT_USERNAME", value:  local.mongo_infos.admin},
        { name: "MONGO_INITDB_ROOT_PASSWORD", value:  local.mongo_infos.admin_pwd },
        { name: "MONGO_API_DB", value:  local.mongo_infos.db },
        { name: "MONGO_API_DB_USERNAME", value: local.mongo_infos.user},
        { name: "MONGO_API_DB_PASSWORD", value: local.mongo_infos.userpwd},
        { name: "MONGO_TEST_DB", value: local.mongo_infos.db_test},
      ]
      portMappings = [{
        containerPort = var.api_service_infos.mongo_ct.port
        hostPort      = 0
      }]
      healthCheck = {
        retries: 3
        command = [ "CMD-SHELL","mongo --eval 'db.runCommand(`ping`).ok' 127.0.0.1:${var.api_service_infos.mongo_ct.port}/test --quiet || exit 1" ]
        timeout = 10
        interval = 60
        startPeriod = 10
      }
      logConfiguration = {
        logDriver = "awslogs"
        options = {
          awslogs-group = var.log_group_name
          awslogs-region = data.aws_region.current.name
          awslogs-stream-prefix = "mongo"
        }
      }
    }
  ])
}

resource "aws_ecs_task_definition" "task_front" {
  family = "${var.front_service_infos.prefix_name}-task-${var.front_service_infos.suffix_name}"
  execution_role_arn = aws_iam_role.ecs_task_execution.arn
  network_mode = "bridge"
  cpu = 300
  memory = 300
  requires_compatibilities = ["EC2"]
  container_definitions = jsonencode([
    {
      name      = var.front_service_infos.front_ct.name
      image     = var.front_service_infos.front_ct.image
      essential = true
      portMappings = [{
          containerPort = var.front_service_infos.front_ct.port
          hostPort      = 0
      }]
      healthCheck = {
        retries: 3
        command =[ "CMD-SHELL","curl -f http://127.0.0.1:${var.front_service_infos.front_ct.port} || exit 1" ]
        timeout = 15
        interval = 45
        startPeriod = 10
      }
      logConfiguration = {
        logDriver = "awslogs"
        options = {
          awslogs-group = var.log_group_name
          awslogs-region = data.aws_region.current.name
          awslogs-stream-prefix = "front"
        }
      }
      environment = [
        { name: "FRONTEND_PORT", value: tostring(var.front_service_infos.front_ct.port)},
      ]
    }
  ])
}



locals {
  services_list = [
    {
      name = "${var.api_service_infos.prefix_name}-service-${var.api_service_infos.suffix_name}"
      task_definition = aws_ecs_task_definition.task_api.arn
      container_port = var.api_service_infos.api_ct.port
      container_name = var.api_service_infos.api_ct.name
      target_group_arn = var.api_service_infos.target_group_arn
    },
    {
      name = "${var.front_service_infos.prefix_name}-service-${var.front_service_infos.suffix_name}"
      task_definition = aws_ecs_task_definition.task_front.arn
      container_port = var.front_service_infos.front_ct.port
      container_name = var.front_service_infos.front_ct.name
      target_group_arn = var.front_service_infos.target_group_arn
    }
  ]
}
resource "aws_ecs_service" "service_generic" {
  for_each =  {for service in local.services_list:  service.name => service}
  #J'ai créer une MAP dynamiquement avec comme clé "service.name" qui contiendra la data du service
  name = each.value["name"]
  cluster = var.cluster_id
  task_definition = each.value["task_definition"]
  desired_count = 1
  scheduling_strategy = "REPLICA"
  launch_type = "EC2"
  deployment_maximum_percent = 200
  deployment_minimum_healthy_percent = 100
  health_check_grace_period_seconds = 30
  ordered_placement_strategy {
    type = "binpack"
    field = "memory"
  }
  deployment_circuit_breaker {
    enable   = true
    rollback = false
  }
  deployment_controller {
    type = "ECS"
  }
  load_balancer {
    container_name = each.value["container_name"]
    container_port = each.value["container_port"]
    target_group_arn = each.value["target_group_arn"]
  }
  iam_role = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/aws-service-role/ecs.amazonaws.com/AWSServiceRoleForECS"
}


/*----------IAM Roles ----------------*/
resource "aws_iam_role" "ecs_task_execution" {
  name = "role-ecs-task-execution-${var.ressource_name_suffix}"
  description = "Provides access to other AWS service resources that are required to run Amazon ECS tasks"
  managed_policy_arns = ["arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"]
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [{
      Effect : "Allow"
      Action = "sts:AssumeRole"
      Principal = {
        Service = "ecs-tasks.amazonaws.com"
      }
    }]
  })
}

