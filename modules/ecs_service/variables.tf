variable "project_name" {
  type = string
  description = "project name"
}

variable "node_env" {
  type = string
  description = "nodejs environment"
}

variable "api_name" {
  type = string
  description = "api name for url path"
}

variable "api_service_infos" {
  type = object({
    prefix_name = string
    suffix_name = string
    target_group_arn = string
    api_ct = object({
      name = string
      image = string
      port = number
    })
    mongo_ct = object({
      name = string
      image = string
      port = number
    })
  })
  description = "api service informations"
}

variable "front_service_infos" {
  type = object({
    prefix_name = string
    suffix_name = string
    target_group_arn = string
    front_ct = object({
      name = string
      image = string
      port = number
    })
  })
  description = "front service informations"
}

variable "log_group_name" {
  type = string
  description = "name of log group to use"
}

variable "cluster_id" {
  type = string
  description = "id of ecs cluster"
}

variable "ressource_name_suffix" {
  type = string
  description = "suffix for name uniques ressources"
}


/*------------Tags--------------*/
variable "resources_tags" {
  type = map(string)
  description = "common tags for all resources"
  default = {}
}

