variable "vpc_id" {
  type = string
  description = "vpc id of stack"
}

variable "cluster_name" {
  type = string
  description = "cluster name"
}

variable "ami_id" {
  type = string
  description = "ami id for ecs instances"
}

variable "instance_type" {
  type = string
  description = "instance type ex: t2.micro"
}

variable "key_name" {
  type = string
  description = " Name of '.pem' key pair for ec2 instances for ssh authentification"
}

variable "sg_ids" {
  type = list(string)
  description = "ids of security groups for ec2 lauchn-template"
}

variable "subnets_ids" {
  type = list(string)
  description = "ids of public subnets for ec2 instances"
}


variable "scheduling_rule_name" {
  type = string
  description = "name of lambda rule for scheduling"
  default = "none"
}

variable "ressource_name_suffix" {
  type = string
  description = "suffix for 'Name' tag"
  default = "x"
}


/*------------Tags--------------*/
variable "resources_tags" {
  type = map(string)
  description = "common tags for all resources"
  default = {}
}
