FROM hashicorp/terraform:1.1.9 AS build
RUN apk add --no-cache curl jq
WORKDIR /terraform
COPY ./environments ./environments
COPY ./modules  ./modules
ENTRYPOINT [""]



